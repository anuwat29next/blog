PROJECT_NAME := oscar_blog_tutorial
init:
	@echo "Building db image..."
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) pull db
	@echo "Building web image"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) build web
	@echo "Building containers..."
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) create
	make start migrate init_user
migrate:
	@echo "Migrating..."
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) exec web python manage.py migrate
init_user:
	@echo "Creating Super User..."
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) exec web python manage.py createsuperuser --noinput
start:
	@echo "Starting Services..."
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) start
stop:
	@echo "Stopping Services..."
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) stop
restart:
	@echo "Restarting Service..."
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) restart
cleanup:
	@echo "Cleaning up..."
	docker-compose -f docker/docker-compose.yml -p $(PROJECT_NAME) down --rmi all -v
